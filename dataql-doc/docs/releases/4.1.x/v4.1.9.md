---
id: v4.1.9
title: v4.1.9 (2020-06-29)
---

# v4.1.9 (2020-06-29)

## 新增
- [Issue I1IT82](https://gitee.com/zycgit/hasor/issues/I1IT82) Api列表时显式其http method
- [Issue I1J2BJ](https://gitee.com/zycgit/hasor/issues/I1J2BJ) Dataway string 库函数新增 split 方法。
- [Issue I1JA0Q](https://gitee.com/zycgit/hasor/issues/I1JA0Q) UI 的操作提供权限控制扩展能力
- 新版本检查提示功能，例如:官方在发布新版本的时。Interface-UI 会检测是否有新版本并弹窗提示给使用者，使用者可以选择永久忽略这次版本提示。
- [Issue I1EQCS](https://gitee.com/zycgit/hasor/issues/I1EQCS) 异常信息增加了行号。
- [Issue I1GZZM](https://gitee.com/zycgit/hasor/issues/I1GZZM) 新增一个开源，Parameters中结构化的参数可以包装为一个整体。

## 优化
- 所有编辑器统一使用 MonacoEditor，去掉 CodeMirror 依赖。
- ApiInfo 类型中增加 CallSource 枚举，用来表示请求的来源。isPerform 方法成为过期方法。
- ValueModel 的isByte、isShort、isInt、isLong 等等方法。逻辑调整为是否可以转换为该类型。
- 去掉了 DataQL 的环境隔离能力。
- FxWebInterceptor 对于 header、cookie 的获取通过 HttpParameters 类来获取。
- [Issue I1LC53](https://gitee.com/zycgit/hasor/issues/I1LC53) 整合Swagger 之后 Header 无法通过 Swagger 传入。

## 修复
- [Issue I1J7K3](https://gitee.com/zycgit/hasor/issues/I1J7K3) 修复 Dataway 生成的 Swagger api文档，在容器中用 swagger 访问失败。
- [Issue I1K85T](https://gitee.com/zycgit/hasor/issues/I1K85T) 修复 字符串加法计算特定场景下抛异常。
- [Issue I1K1MJ](https://gitee.com/zycgit/hasor/issues/I1K1MJ) 修复 @@mybatis 多线程并发问题 MybatisFragment
- [Issue I1J33N](https://gitee.com/zycgit/hasor/issues/I1J33N) 跨域下 4.1.7、4.1.8 前端请求第一次是options时会报错。
