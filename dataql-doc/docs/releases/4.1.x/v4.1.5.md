---
id: v4.1.5
title: v4.1.5 (2020-05-09)
---

# v4.1.5 (2020-05-09)

## 新增
- [Issue I1FGQO](https://gitee.com/zycgit/hasor/issues/I1FGQO) 接口可以自定义返回值，完美兼容老项目的接口规范。
- [Issue 32](https://github.com/zycgit/hasor/issues/32) 增加 ConvertUdfSource 类型转换函数包。

## 优化
- FxSql 工具类抽象出 FxQuery 接口 和 SqlFxQuery 实现类。
- [Issue 30](https://github.com/zycgit/hasor/issues/30) 优化了一下交互，新建接口情况下 comment 不会主动显示出来。

## 修复
- [Issue I1G6QS](https://gitee.com/zycgit/hasor/issues/I1G6QS) DatawayService 接口使用 @Bean 在 Spring 中配置报错。
- [Issue 29](https://github.com/zycgit/hasor/issues/29) SQL模式下保存api之后，点击编辑再进入，没有显示之前保存的信息。
- [Issue 31](https://github.com/zycgit/hasor/issues/31) 分页模式下，FRAGMENT_SQL_COLUMN_CASE 不起作用。